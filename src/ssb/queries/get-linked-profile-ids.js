const pull = require('pull-stream')
const pullSort = require('pull-sort')
const paraMap = require('pull-paramap')

const GetLinkedProfiles = require('./get-linked-profiles')

module.exports = function GetLinkedProfileIds (ssb) {
  const getLinkedProfiles = GetLinkedProfiles(ssb)

  return function getLinkedProfileIds (cb) {
    getLinkedProfiles((err, profiles) => {
      if (err) return cb(err)

      pull(
        pull.values(profiles),
        paraMap(
          (profile, cb) => cb(null, profile.key),
          5
        ),
        pullSort(),
        pull.collect(cb)
      )
    })
  }
}
