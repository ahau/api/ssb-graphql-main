const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const { where, type, toPullStream } = require('ssb-db2/operators')

module.exports = function GetLinkedProfiles (ssb) {
  // finds all the profile/community with the given poBoxId
  // and uses one of them to get the recps for the groupId
  function findGroupIdByPOBoxId (poBoxId, cb) {
    pull(
      ssb.db.query(
        where(type('profile/community')),
        toPullStream()
      ),
      pull.filter(m => m.value.content?.tangles?.profile?.root === null),
      pull.filter(m => m.value.content?.poBoxId?.set === poBoxId),
      pull.filter(m => m.value.content?.recps),
      pull.take(1),
      pull.collect((err, profiles) => {
        if (err) return cb(err)

        if (!profiles || !profiles.length) return cb(null, null)

        cb(null, profiles[0].value.content.recps[0])
      })
    )
  }

  function findGroupId (profile, cb) {
    if (profile.type !== 'person/admin') {
      return cb(null, profile.recps && profile.recps[0])
    }

    findGroupIdByPOBoxId(profile.recps[0], cb)
  }

  return function (cb) {
    ssb.profile.findByFeedId(ssb.id, (err, profiles) => {
      if (err) return cb(err)

      const allProfiles = [
        ...profiles.public,
        ...profiles.private
      ]

      pull(
        pull.values(allProfiles),
        pull.filter(profile => profile.tomstone !== null),
        paraMap(
          (profile, cb) => {
            // keep public profiles with no recps
            if (profile.recps === null) return cb(null, profile)

            // keep source profiles
            if (profile.type === 'person/source') return cb(null, profile)

            findGroupId(profile, (err, groupId) => {
              if (err) return cb(err)
              if (!groupId) return cb(null, null)

              // find the community profiles linked to the groupId
              ssb.profile.findByGroupId(groupId, (err, profiles) => {
                if (err) return cb(err)

                if (!profiles.private.length) return cb(null, null)
                if (!profiles.public.length) return cb(null, null)

                if (profiles.private[0].tomstone) return cb(null, null)
                if (profiles.public[0].tombstone) return cb(null, null)

                cb(null, profile)
              })
            })
          },
          5
        ),
        pull.filter(Boolean),
        pull.collect(cb)
      )
    })
  }
}
