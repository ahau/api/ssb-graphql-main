const { promisify } = require('util')

module.exports = function LoadPersonalSettings (ssb) {
  return function loadPersonalSettings (groupId, cb) {
    if (cb === undefined) return promisify(loadPersonalSettings)(groupId)

    ssb.settings.findByFeedId(ssb.id, (err, settings) => {
      if (err) return cb(err)

      const personalSettings = settings.filter(s => {
        return (
          s.recps.length === 1 &&
          s.recps[0] === groupId
        )
      })

      if (personalSettings.length) {
        if (personalSettings.length > 1) console.warn('ssb-graphql/main found multiple personal settings, using first one')
        return cb(null, personalSettings[0].key)
      }

      const details = {
        authors: {
          add: [ssb.id]
        },
        recps: [groupId]
      }

      ssb.settings.create(details, (err, settingsId) => {
        if (err) return cb(err)

        ssb.settings.link.create({ settings: settingsId }, (err, link) => {
          if (err) return cb(err)

          cb(null, settingsId)
        })
      })
    })
  }
}
