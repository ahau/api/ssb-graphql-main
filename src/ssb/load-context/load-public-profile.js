const { promisify } = require('util')

/* eslint-disable brace-style */
module.exports = function LoadPublicProfile (ssb) {
  return function loadPublicProfile (profiles, type, cb) {
    if (cb === undefined) return promisify(loadPublicProfile)(type)

    const publicProfiles = profiles.filter(p => p.type === type)

    if (publicProfiles.length) {
      if (publicProfiles.length > 1) console.warn('ssb-graphql/main found multiple public profiles, using first one')

      cb(null, publicProfiles[0].key)
    }
    else createPublicProfile(type, cb)
  }

  function createPublicProfile (type, cb) {
    if (!(ssb.profile[type] && ssb.profile[type].public)) return cb(new Error(`no profile handler for type ${type}`))

    const details = {
      authors: {
        add: [ssb.id]
      }
    }
    if (ssb.recpsGuard) details.allowPublic = true

    ssb.profile[type].public.create(details, (err, profileId) => {
      if (err) return cb(err)

      ssb.profile.link.create({ profileId, allowPublic: true }, (err, link) => {
        if (err) return cb(err)

        cb(null, profileId)
      })
    })
  }
}
