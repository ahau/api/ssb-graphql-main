# Changelog | @ssb-graphql/main

## v9.3.0

- `hyperBlobsAutoPruneConfig` - query that gets hyperBlobs.autoPrune config
- `saveHyperBlobsAutoPruneConfig` - mutation that sets hyperBlobs.autoPrune config

## v9.0.0

- Removed `graphql-upload` to fix file upload bug, added a temporary solution which bipasses checking the value when parsing Upload input
  - see `src/typeDefs.js`
  - NOTE: previous tests are currently failing

## v8.0.0

- Auto adds settings record with loadContext
