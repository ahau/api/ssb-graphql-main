const tape = require('tape')
const { generate } = require('ssb-keys')
const { isFeed, isMsg } = require('ssb-ref')

const { SSB } = require('../test-bot')
const LoadContext = require('../../src/ssb/load-context')

tape('load-context (type: pataka, no tribes)', (t) => {
  const name = 'load-context-' + Date.now()
  const keys = generate()

  let ssb = SSB({ name, keys, startUnclean: true, installTribes: false })
  let loadContext = LoadContext(ssb, { type: 'pataka' })

  loadContext((err, context) => {
    if (err) throw err

    t.true(isFeed(context.public.feedId), 'has feedId')
    t.true(isMsg(context.public.profileId), 'has public.profileId')

    ssb.profile.pataka.public.get(context.public.profileId, (err, profile) => {
      if (err) throw err
      const { recps, type } = profile
      t.deepEqual(recps, null, 'public profile (no recps)')
      t.deepEqual(type, 'pataka', 'is profile/pataka')

      t.equal(context.personal, undefined, 'no personal info')

      ssb.close(() => {
        checkPersisted(context)
      })
    })
  })

  function checkPersisted (persistedContext) {
    ssb = SSB({ name, keys, startUnclean: true, installTribes: false })
    loadContext = LoadContext(ssb, { type: 'pataka' })

    loadContext((err, context) => {
      if (err) throw err
      t.deepEqual(context, persistedContext, 'context persisted!')

      ssb.close()
      t.end()
    })
  }
})
