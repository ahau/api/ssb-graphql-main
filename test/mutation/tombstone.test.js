const tape = require('tape')
const { promisify: p } = require('util')
const TestBot = require('../test-bot')

function handleErr (err) {
  console.log(JSON.stringify(err, null, 2))
  return ({ errors: [err], err })
}

tape('tombstone', async (t) => {
  const { ssb, apollo } = await TestBot({ profile: true })
  const { groupId } = await p(ssb.tribes.create)({})

  async function savePerson (input) {
    const res = await apollo.mutate({
      mutation: `
        mutation($input: PersonProfileInput!) {
          savePerson(input: $input)
        }
      `,
      variables: {
        input
      }
    })
      .catch(handleErr)

    t.error(res.errors, 'saves person/profile without errors')

    return res.data.savePerson
  }

  async function getProfile (id) {
    const res = await apollo.query({
      query: `
        query($id: String!) {
          profile(id: $id) {
            type
            preferredName
            tombstone {
              date
              reason
            }
            recps
          }
        }
      `,
      variables: {
        id
      }
    })

    t.error(res.errors, 'gets profile without errors')

    return res.data.profile
  }

  const initalInput = {
    type: 'person',
    preferredName: 'Cherese',
    authors: {
      add: [ssb.id]
    },
    recps: [groupId]
  }

  const id = await savePerson(initalInput)
  const profile = await getProfile(id)

  t.deepEqual(
    profile,
    {
      type: 'person',
      preferredName: 'Cherese',
      tombstone: null,
      recps: [groupId]
    },
    'returns correct state -> tombstone is null'
  )

  // update the profile with a tombstone
  await savePerson({
    id, // to update this profile
    tombstone: {
      // date,
      // NOTE not actually saved as this date
      reason: 'no longer needed'
    }
  })

  // get the updated profile
  const tombstonedProfile = await getProfile(id)

  t.deepEqual(
    tombstonedProfile,
    {
      type: 'person',
      preferredName: 'Cherese',
      tombstone: {
        date: tombstonedProfile.tombstone.date,
        // NOTE had to fake this
        reason: 'no longer needed'
      },
      recps: [groupId]
    }
  )

  ssb.close()
  t.end()
})
