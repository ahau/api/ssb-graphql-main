/*eslint-disable */
const test = require('tape')
const fs = require('fs')
const http = require('http')
const { isBlob } = require('ssb-ref')
const pull = require('pull-stream')
const toStream = require('pull-stream-to-stream')
const TestBot = require('../test-bot')

const dummyFile = () => {
  // GraphQL file uploads are complex. Fake it!
  // The only methods we look for are `createReadStream()` and `mimeType`.
  // This uploads the current file (yes, the one you're reading now).
  //
  return new Promise((resolve) => {
    resolve({
      __typename: 'Upload',
      createReadStream: () => fs.createReadStream(__filename),
      mimeType: 'text/plain'
    })
  })
}

test('uploadFile (unencrypted)', { todo: true }, async (t) => {
  console.log('WARNING!!!!  test currently broken, needs fixing')
  t.end()
  return

  const { ssb, apollo } = await TestBot() // eslint-disable-line
  // Read the actual contents of this file for later comparison.
  const fileContents = fs.readFileSync(__filename, 'utf8')

  const result = await apollo.mutate({
    mutation: `mutation($file: Upload!, $size: Int!) {
      uploadFile(file: $file, size: $size) {
        blobId
        mimeType
        size
        uri
      }
    }`,
    variables: {
      size: 1 * 1024 * 1024,
      file: dummyFile()
    }
  }).catch(err => ({ errors: err }))
  if (result.errors) console.log(JSON.stringify(result.errors, null, 2))

  t.error(result.errors, 'mutation should not return errors')
  t.equals(typeof result.data, 'object', 'result.data is an object')
  t.equals(
    typeof result.data.uploadFile,
    'object',
    'result.data.uploadFile is an object'
  )
  t.equals(
    typeof result.data.uploadFile.uri,
    'string',
    'result.data.uploadFile.uri is a string'
  )
  t.equals(
    typeof result.data.uploadFile.size,
    'number',
    'result.data.uploadFile.size is a number'
  )
  t.equals(
    result.data.uploadFile.size,
    fileContents.length,
    'result.data.uploadFile.size matches upload size'
  )

  http
    .get(result.data.uploadFile.uri, (res) => {
      const data = []
      res
        .on('data', (chunk) => data.push(chunk))
        .on('end', () => {
          // Ensure that the blob matches this file's contents exactly.
          t.equals(
            data.join(''),
            fileContents,
            'blob upload matches original file'
          )
        })
      ssb.close(t.end)
    })
    .on('error', t.error)
    .end()
})

test('uploadFile (encrypted)', { todo: true }, async (t) => {
  console.log('TODO - fix')
  t.end()
  return

  const { ssb, apollo } = await TestBot() // eslint-disable-line
  t.plan(10)
  // Read the actual contents of this file for later comparison.
  const fileContents = fs.readFileSync(__filename, 'utf8')

  const result = await apollo.mutate({
    mutation: `mutation($file: Upload!, $size: Int!, $encrypt: Boolean) {
      uploadFile(file: $file, size: $size, encrypt: $encrypt) {
        blobId
        mimeType
        uri
        size

        ...on BlobScuttlebutt {
          unbox
        }
      }
    }`,
    variables: {
      file: dummyFile(),
      size: 1 * 1024 * 1024,
      encrypt: true
    }
  })

  t.error(result.errors, 'mutation should not return errors')
  t.equals(typeof result.data, 'object', 'result.data is an object')
  t.equals(
    typeof result.data.uploadFile,
    'object',
    'result.data.uploadFile is an object'
  )

  const { blobId, mimeType, size, unbox, uri } = result.data.uploadFile
  t.true(isBlob(blobId), 'data.uploadFile.blobId is a blob ref')
  t.true(mimeType.match(/\w+\/\w+/), 'data.uploadFile.mimeType is a mimeType')

  t.equals(typeof size, 'number', 'data.uploadFile.size is a number')
  t.equals(typeof unbox, 'string', 'data.uploadFile.unbox is a string')

  t.equals(typeof uri, 'string', 'data.uploadFile.uri is a string')
  t.ok(uri.indexOf('?unbox=') > -1, 'URI has unbox key')

  http
    .get(result.data.uploadFile.uri, (res) => {
      const data = []
      res
        .on('data', (chunk) => data.push(chunk))
        .on('end', () => {
          // Ensure that the blob matches this file's contents exactly.
          t.equals(
            data.join(''),
            fileContents,
            'blob upload matches original file'
          )
          ssb.close(t.end)
        })
    })
    .on('error', t.error)
    .end()
})

test('uploadFile (hyper-blobs)', { todo: true }, async (t) => {
  console.log('TODO - fix')
  t.end()
  return

  const { ssb, apollo } = await TestBot({ installHyperBlobs: true }) // eslint-disable-line
  t.plan(11)

  const bytes32 = 'boots&cats&boots&cats&boots&bat,'
  const kilobytes64 = new Array(1024).fill(bytes32 + bytes32).join('')
  const data = new Array(10 * 1024 * 1024 / kilobytes64.length).fill(kilobytes64)
  data.push('dog')

  // GraphQL file uploads are complex. Fake it!
  // The only methods we look for are `createReadStream()` and `mimeType`.
  const file = new Promise((resolve) => {
    resolve({
      createReadStream: () => {
        return toStream.source(pull.values(data))
      },
      mimeType: 'text/plain'
    })
  })

  const result = await apollo.mutate({
    mutation: `mutation($file: Upload!, $size: Int!) {
      uploadFile(file: $file, size: $size) {
        type
        blobId
        mimeType
        size
        uri

        ...on BlobScuttlebutt {
          unbox
        }

        ...on BlobHyper {
          driveAddress
          readKey
        }
      }
    }`,
    variables: {
      file,
      size: 10 * 1024 * 1024 // over the 5MB default max
    }
  })

  t.error(result.errors, 'mutation should not return errors')
  t.equals(typeof result.data, 'object', 'result.data is an object')
  t.equals(
    typeof result.data.uploadFile,
    'object',
    'result.data.uploadFile is an object'
  )

  const { type, mimeType, size, driveAddress, blobId, readKey, uri } = result.data.uploadFile

  t.equals(type, 'hyper', 'data.uploadFile.type is correct')
  t.equals(typeof size, 'number', 'data.uploadFile.size is a number')
  t.true(mimeType.match(/\w+\/\w+/), 'data.uploadFile.mimeType is a mimeType')

  t.equals(Buffer.from(driveAddress, 'base64').length, 32, 'data.uploadFile.driveAddress is a 32 byte base64 encoded string')
  t.equals(typeof blobId, 'string', 'data.uploadFile.blobId is a string')
  t.equals(Buffer.from(readKey, 'base64').length, 32, 'data.uploadFile.readKey is a 32 byte base64 encoded string')

  t.equals(typeof uri, 'string', 'data.uploadFile.uri is a string')

  http
    .get(result.data.uploadFile.uri, (res) => {
      const _data = []
      res
        .on('data', (chunk) => { _data.push(chunk) })
        .on('end', () => {
          // Ensure that the blob matches this file's contents exactly.
          t.equals(
            _data.join(''),
            data.join(''),
            'blob upload matches original file'
          )
          ssb.close(t.end)
        })
    })
    .on('error', t.error)
    .end()
})

test('uploadFile (no ssb-blobs installed)', { todo: false }, async (t) => {
  console.log('WARNING!!!!  test currently broken, needs fixing')
  t.end()
  return

  const { ssb, apollo } = await TestBot({
    installBlobs: false,
    installHyperBlobs: true
    // blobs: {
    //   max: 0 // i.e. none go to ssb-blobs
    // }
  })

  const result = await apollo.mutate({
    mutation: `mutation($file: Upload!, $size: Int!) {
      uploadFile(file: $file, size: $size) {
        type
        blobId
        mimeType
        size
        uri

        ...on BlobScuttlebutt {
          unbox
        }

        ...on BlobHyper {
          driveAddress
          readKey
        }
      }
    }`,
    variables: {
      file: dummyFile(),
      size: 1 * 1024 * 1024
    }
  })

  t.error(result.errors, 'mutation should not return errors')
  t.equals(typeof result.data, 'object', 'result.data is an object')
  t.equals(
    typeof result.data.uploadFile,
    'object',
    'result.data.uploadFile is an object'
  )

  const { type } = result.data.uploadFile

  t.equals(type, 'hyper', 'data.uploadFile.type is correct')

  ssb.close(t.end)
})

