const tape = require('tape')
const TestBot = require('../test-bot')

tape('hyperblob.autoPrune', async t => {
  const { ssb, apollo } = await TestBot({
    hyperBlobs: {
      autoPrune: true
    },
    installHyperBlobs: true
  })

  async function getHyperBlobsAutoPruneConfig () {
    const res = await apollo.query({
      query: `
        query {
          hyperBlobsAutoPruneConfig {
            intervalTime
            startDelay
            maxRemoteSize
          }
        }
      `
    })
      .catch(err => {
        console.error('woops', err)
        return { errors: err }
      })

    t.error(res.errors, 'can query hyperBlobsAutoPruneConfig')

    return res.data.hyperBlobsAutoPruneConfig
  }

  async function saveHyperBlobsAutoPruneConfig (input) {
    const res = await apollo.mutate({
      mutation: `
        mutation($disable: Boolean, $startDelay: Float, $maxRemoteSize: Float, $intervalTime: Float) {
          saveHyperBlobsAutoPruneConfig(disable: $disable, startDelay: $startDelay, maxRemoteSize: $maxRemoteSize, intervalTime: $intervalTime)
        }
      `,
      variables: {
        ...input
      }
    })

    t.error(res.errors, 'mutate hyper blob auto-prune config')

    return res.data.saveHyperBlobsAutoPruneConfig
  }

  const result = await getHyperBlobsAutoPruneConfig()

  t.deepEqual(
    result,
    {
      intervalTime: 3600000,
      startDelay: 600000,
      maxRemoteSize: 5368709120
    },
    'returns default auto prune config when autoPrune: true'
  )

  // set hyperBlobs auto-prune config
  const set = await saveHyperBlobsAutoPruneConfig({
    intervalTime: 500,
    startDelay: 0
  })

  t.true(set, 'set hyper blobs auto prune config')

  ssb.close(t.end)
})
