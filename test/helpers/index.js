const { promisify: p } = require('util')
const omit = require('lodash.omit')
const fixInput = require('@ssb-graphql/profile/src/lib/fix-input')

const privateOnlyFields = ['allowWhakapapaViews', 'allowStories', 'allowPersonsList']
const publicOnlyFields = ['joiningQuestions', 'customFields']

function InitGroup (ssb) {
  async function initGroupProfiles (groupId, input) {
    // NOTE: this method does not copy details from your personal profile to the group profiles

    // auto set authors to the creator
    const details = {
      ...fixInput.community(input), // includes poBoxId
      authors: {
        add: [ssb.id]
      }
    }

    // // create the groups public profile
    const publicProfileId = await p(ssb.profile.community.public.create)({ ...omit(details, privateOnlyFields), allowPublic: true })
    await p(ssb.profile.link.create)(publicProfileId, { groupId, allowPublic: true })

    // create the groups private profile
    const groupProfileId = await p(ssb.profile.community.group.create)({ ...omit(details, publicOnlyFields), recps: [groupId] })
    await p(ssb.profile.link.create)(groupProfileId, { groupId })

    return { publicProfileId, groupProfileId }
  }

  return async function initGroup (input) {
    const { groupId } = await p(ssb.tribes.create)({})
    // make a profile for you in this group

    let details = {
      authors: {
        add: [ssb.id]
      },
      recps: [groupId]
    }

    const groupProfileId = await p(ssb.profile.person.group.create)(details)
    await p(ssb.profile.link.create)(groupProfileId)

    // create an admin subgroup
    const { poBoxId } = await p(ssb.tribes.subtribe.create)(groupId, { addPOBox: true, admin: true })

    details = {
      authors: {
        add: [ssb.id, '*']
      },
      recps: [poBoxId, ssb.id]
    }

    const adminProfileId = await p(ssb.profile.person.admin.create)(details)
    await p(ssb.profile.link.create)(adminProfileId)

    input.poBoxId = poBoxId

    const community = await initGroupProfiles(groupId, input)

    return {
      groupId,
      community,
      person: {
        groupProfileId,
        adminProfileId
      }
    }
  }
}

module.exports = {
  InitGroup
}
