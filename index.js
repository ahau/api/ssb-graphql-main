const typeDefs = require('./src/typeDefs')
const Resolvers = require('./src/resolvers')
const LoadContext = require('./src/ssb/load-context')

module.exports = (ssb, opts) => ({
  loadContext: LoadContext(ssb, opts),
  typeDefs,
  resolvers: Resolvers(ssb)
})
